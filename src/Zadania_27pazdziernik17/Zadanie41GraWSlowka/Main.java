package Zadania_27pazdziernik17.Zadanie41GraWSlowka;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Main {

    private static Gracz g1 = new Gracz("Gracz 1");
    private static Gracz g2 = new Gracz("Gracz 2");
    private static Gracz obecnyGracz = g1;
    private static Random random = new Random();
    private static File file1 = new File("/Users/marekdybusc/IdeaProjects/Programowanie1Divo/Programowanie1/src/Zadania_27pazdziernik17/Zadanie41GraWSlowka/Gracz1");
    private static File file2 = new File("/Users/marekdybusc/IdeaProjects/Programowanie1Divo/Programowanie1/src/Zadania_27pazdziernik17/Zadanie41GraWSlowka/Gracz2");

    public static void main(String[] args) {

        readFile(g1.getMap(), file1);
        readFile(g2.getMap(), file2);

        String word = randomLetterWord();

        boolean playing = true;

        while (playing){
            try {
                word = getWord(word, obecnyGracz);
            } catch (IllegalArgumentException e) {
                changePlayer();
                System.out.println("Winner: " + obecnyGracz);
                break;
            }
            System.out.println(obecnyGracz + ": "+ word);
            changePlayer();
        }




    }

    private static String randomLetterWord() {
        String alphabet = "łcdghjkmnopstz";
        StringBuilder builder = new StringBuilder();
        builder.append(alphabet.charAt(random.nextInt(alphabet.length())));
        return builder.toString();
    }

    private static void readFile(Map<Character, ArrayList<String>> map, File file) {
        try(Scanner skan = new Scanner(file)) {
            while (skan.hasNextLine()){
                String word = skan.nextLine();
                if(map.containsKey(word.charAt(0))){
                    map.get(word.charAt(0)).add(word);
                }
                else {
                    ArrayList<String> al = new ArrayList<>();
                    al.add(word);
                    map.put(word.charAt(0), al);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void changePlayer(){
        if (obecnyGracz == g1) obecnyGracz = g2;
        else obecnyGracz = g1;
    }

    private static String getWord(String string, Gracz gracz){
            char letter = string.charAt(string.length()-1);

        if (gracz.getMap().containsKey(letter)) {
            if (!gracz.getMap().get(letter).isEmpty()) {
                ArrayList<String> theList = gracz.getMap().get(letter);
                String word = theList.get(random.nextInt(theList.size()));
                theList.remove(word);
                return word;
            } else {
                gracz.getMap().remove(letter);
                throw new IllegalArgumentException();
            }
        }
        else throw new  IllegalArgumentException();
    }
}


