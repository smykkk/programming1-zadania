package Zadania_27pazdziernik17.Zadanie41GraWSlowka;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Gracz {
    private Map<Character, ArrayList<String>> charToWordListMap = new HashMap<>();
    private String name;

    public Gracz(String name) {
        this.name = name;
    }

    public Map<Character, ArrayList<String>> getMap() {
        return charToWordListMap;
    }

    @Override
    public String toString() {
        return name;
    }

}
