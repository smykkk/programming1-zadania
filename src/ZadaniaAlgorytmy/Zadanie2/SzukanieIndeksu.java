package ZadaniaAlgorytmy.Zadanie2;

public class SzukanieIndeksu {
    public static void main(String[] args) {
        int[] tablica = {1,5,7,9,11,12,13,15,17,20,25,26,29,31,33,39,50,52,54,66,78,83,86};
        System.out.println("Method 1:");
        System.out.println("Index: " + getIndex(tablica, 13));
        System.out.println();
        System.out.println("Method 2:");
        System.out.println("Index: " + getIndex2(tablica, 13));

    }
    private static int getIndex(int[] tablica, int wartosc){
        int counter = 0;
        for (int i = 0; i < tablica.length; i++) {
            counter++;
            if(tablica[i] == wartosc){
                System.out.println("Counter: " + counter);
                return i;
            }
        }
        return -0;
    }
    private static int getIndex2(int[] tablica, int wartosc){
        int counter = 0;
        boolean searching = true;
        int min = 0;
        int max = tablica.length;
        int mid;
        while (searching){
            mid = min + (max - min) /2;
            if (wartosc > tablica[mid]){
                min = mid;
            }
            if (wartosc < tablica[mid]){
                max = mid;
            }
            else if(wartosc == tablica[mid]){
                System.out.println("Counter: " + counter);
                return mid;
            }
            counter++;
        }
        System.out.println("Counter: " + counter);
        return 0;
    }
}
