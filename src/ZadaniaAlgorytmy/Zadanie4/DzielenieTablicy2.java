package ZadaniaAlgorytmy.Zadanie4;

import java.util.*;

public class DzielenieTablicy2 {
    private static List<Integer> whole = new ArrayList<>(Arrays.asList(32, 12, 1, 6, 11, 90, 5, 2, 9, 23, 8));
    private static List<Integer> set1 = new  ArrayList<> (whole); //tworzy kopie listy
    private static List<Integer> set2 = new ArrayList<>();

    public static void main(String[] args) {
        Map<Integer, Integer> valueDifferenceMap = new HashMap<>();
        int currentDifference = getDifference();
        int newDifference = currentDifference - 1;
        int value = whole.get(0);

        //kiedy przesuniecie wybranego elementu powoduje mniejsza roznice niz obecna
        while (currentDifference > newDifference) {

            //przesun wybrany element i zapisz roznice jako obecna
            moveValue(value);
            currentDifference = getDifference();

            System.out.println(set1);
            System.out.println(set2);

            //sprawdz roznice dla przesuniecia kazdego z elementow i zapisz do mapy
            fillMap(valueDifferenceMap);

            //wybierz z mapy element powodujacy najmniejsza roznice
            Integer minimumDifference = currentDifference;
            for (Map.Entry<Integer, Integer> mapEntry: valueDifferenceMap.entrySet()){
                if(minimumDifference > mapEntry.getValue()){
                    minimumDifference = mapEntry.getValue();
                    value = mapEntry.getKey();
                }
            }
            System.out.println(valueDifferenceMap);
            //zapisz roznice w razie przesuniecia wybranego elementu
            newDifference = minimumDifference;
            valueDifferenceMap.clear();
        }
        System.out.println();
        System.out.println(set1);
        System.out.println(set2);
        System.out.println("Current difference: " + currentDifference);
    }

    private static int fillMap(Map<Integer, Integer> valueDifferenceMap) {
        int value = 0;
        for (int i = 0; i < whole.size(); i++) {
            value = whole.get(i);
            moveValue(value);
            valueDifferenceMap.put(value, getDifference());
            moveValue(value);
        }
        return value;
    }

    public static int getDifference(){
            int set1Sum = 0;
            int set2Sum = 0;
            for (int i = 0; i < set1.size(); i++) {
                set1Sum += set1.get(i);
            }
            for (int i = 0; i < set2.size(); i++) {
                set2Sum += set2.get(i);
            }
            return Math.abs(set1Sum - set2Sum);
    }

    private static void moveValue(Integer value){
        if(set1.contains(value)){
            set1.remove(value);
            set2.add(value);
        }
        else if (set2.contains(value)){
            set2.remove(value);
            set1.add(value);
        }
    }
    }
