package Zadania_26pazdziernik17.Zajęcia.Zadanie2;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Listdifference {

    private static List<Integer> linkedList = new LinkedList<>();
    private static List<Integer> arraylist = new ArrayList<>();
//    100000
    //wstawia na poczatek
    //wstawia w srodek
    //wstawia na koniec
    public static void main(String[] args) {
        Random random = new Random();


        System.out.println("Linked@END start: " + LocalDateTime.now());
        for (int i = 0; i < 100000; i++) {
            int liczba = random.nextInt();
            linkedList.add(liczba);
        }
        System.out.println("Linked@END finish: " + LocalDateTime.now());
        System.out.println("Array@END start: " + LocalDateTime.now());
        for (int i = 0; i < 100000; i++) {
            int liczba = random.nextInt();
            arraylist.add(liczba);
        }
        System.out.println("Array@END finish: " + LocalDateTime.now());

        System.out.println("Linked@BEGINNING start: " + LocalDateTime.now());
        for (int i = 0; i < 100000; i++) {
            int liczba = random.nextInt();
            linkedList.add(0, liczba);
        }
        System.out.println("Linked@BEGINNING finish: " + LocalDateTime.now());
        System.out.println("Array@BEGINNING start: " + LocalDateTime.now());
        for (int i = 0; i < 100000; i++) {
            int liczba = random.nextInt();
            arraylist.add(0, liczba);
        }
        System.out.println("Array@BEGINNING finish: " + LocalDateTime.now());

        System.out.println("Linked@MID start: " + LocalDateTime.now());
        for (int i = 0; i < 100000; i++) {
            int liczba = random.nextInt();
            linkedList.add(linkedList.size()/2, liczba);
        }
        System.out.println("Linked@MID finish: " + LocalDateTime.now());
        System.out.println("Array@MID start: " + LocalDateTime.now());
        for (int i = 0; i < 100000; i++) {
            int liczba = random.nextInt();
            arraylist.add(linkedList.size()/2, liczba);
        }
        System.out.println("Array@MID finish: " + LocalDateTime.now());

        System.out.println();

        ////////////////////////////////////////////////////////////////

        System.out.println("Linked@GetRandom start: " + LocalDateTime.now());
        for (int i = 0; i < 100000; i++) {
            int liczba = random.nextInt(10000);
            linkedList.get(liczba);
        }
        System.out.println("Linked@GetRandom finish: " + LocalDateTime.now());
        System.out.println("Array@GetRandom start: " + LocalDateTime.now());
        for (int i = 0; i < 100000; i++) {
            int liczba = random.nextInt(10000);
            arraylist.get(liczba);
        }
        System.out.println("Array@GetRandom finish: " + LocalDateTime.now());

        System.out.println("Linked@GetEnd start: " + LocalDateTime.now());
        for (int i = 0; i < 100000; i++) {
            linkedList.get(linkedList.size()-1);
        }
        System.out.println("Linked@GetEnd finish: " + LocalDateTime.now());
        System.out.println("Array@GetEnd start: " + LocalDateTime.now());
        for (int i = 0; i < 100000; i++) {
            arraylist.get(linkedList.size()-1);
        }
        System.out.println("Array@GetEnd finish: " + LocalDateTime.now());

        System.out.println("Linked@GetMid start: " + LocalDateTime.now());
        for (int i = 0; i < 100000; i++) {
            linkedList.get(linkedList.size()/2);
        }
        System.out.println("Linked@GetMid finish: " + LocalDateTime.now());
        System.out.println("Array@GetMid start: " + LocalDateTime.now());
        for (int i = 0; i < 100000; i++) {
            arraylist.get(linkedList.size()/2);
        }
        System.out.println("Array@GetMid finish: " + LocalDateTime.now());


    }
}
