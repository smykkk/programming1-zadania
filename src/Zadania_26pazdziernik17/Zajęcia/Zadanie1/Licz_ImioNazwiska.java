package Zadania_26pazdziernik17.Zajęcia.Zadanie1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Licz_ImioNazwiska {
    public static void main(String[] args) {
        Map<String, Integer> imiona = new HashMap<>();
        Map<String, Integer> nazwiska = new HashMap<>();

        readFile(imiona, nazwiska);
        printOccurence(imiona, nazwiska);
    }

    private static void readFile(Map<String, Integer> imiona, Map<String, Integer> nazwiska) {
        File file = new File("/Users/marekdybusc/IdeaProjects/Programowanie1Divo/Programowanie1/src/Zadania_26pazdziernik17/Zajęcia/Zadanie1/Lista.txt");
        try(Scanner skan = new Scanner(file)) {
            while (skan.hasNextLine()){
                String[] line = skan.nextLine().split(" ");
                String imie = toTitleCase(line[1]);
                String nazwisko = toTitleCase(line[0]);
                toMap(imie, imiona);
                toMap(nazwisko, nazwiska);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void toMap(String string, Map<String, Integer> map){
        if(map.containsKey(string)){
            Integer value = map.get(string);
            value++;
            map.put(string, value);
        }
        else map.put(string, 1);
    }

    private static void printOccurence(Map<String, Integer> imiona,Map<String, Integer> nazwiska){
        for (Map.Entry<String, Integer> e : imiona.entrySet()) {
            System.out.println(e.getKey() + " -> wystąpienia: " + e.getValue());
        }
        System.out.println();
        for (Map.Entry<String, Integer> e : nazwiska.entrySet()) {
            String s = e.getKey();
            System.out.println(e.getKey() + " -> wystąpienia: " + e.getValue());
        }
    }

    public static String toTitleCase(String string){
        StringBuilder stringBuilder = new StringBuilder();
        String toupper = string.toUpperCase();
        stringBuilder.append(toupper.charAt(0));
        stringBuilder.append(string.substring(1).toLowerCase());
        return stringBuilder.toString();
    }

}
