package Zadania_26pazdziernik17.Zadanie3Filmy;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Baza baza = new Baza();
        Map<String, Double> filmKasa = new HashMap<>();


        baza.wczytajPliki();
        baza.fillData();

        System.out.println(baza.getFilmy());
        System.out.println(baza.getMiasta());


        for (Miasto m : baza.getBazaMiastoFilmZysk().keySet()) {
            for (Film f : baza.getBazaMiastoFilmZysk().get(m).keySet()) {
                System.out.println(m + ": " + f + ": " + baza.getBazaMiastoFilmZysk().get(m).get(f));

            }
        }

        for (Miasto m : baza.getBazaMiastoFilmZysk().keySet()) {
            long zyskMiasta = 0;
            for (Film f : baza.getBazaMiastoFilmZysk().get(m).keySet()) {
                zyskMiasta += baza.getBazaMiastoFilmZysk().get(m).get(f);
                System.out.println(m + ": " + f + ": " + baza.getBazaMiastoFilmZysk().get(m).get(f));

            }
            System.out.println(m + ": " + zyskMiasta);
        }

        for (Miasto m : baza.getBazaMiastoFilmZysk().keySet()) {
            for (Film f : baza.getBazaMiastoFilmZysk().get(m).keySet()) {
                if(!filmKasa.containsKey(f.getNazwa())) filmKasa.put(f.getNazwa(),
                        baza.getBazaMiastoFilmZysk().get(m).get(f));
                else{
                    double zysk = baza.getBazaMiastoFilmZysk().get(m).get(f) + filmKasa.get(f.getNazwa());
                    filmKasa.put(f.getNazwa(), zysk);
                }
            }

        }

        System.out.println();

        for (String f : filmKasa.keySet()) {
            System.out.println(f + ": " + filmKasa.get(f));
        }

    }
}
