package Zadania_26pazdziernik17.Zadanie3Filmy;

public class Film {
    private String nazwa;
    private int cena;

    public Film(String nazwa) {
        this.nazwa = nazwa;
        this.cena = nazwa.replaceAll(" ", "").length() * 2;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getCena() {
        return cena;
    }

    @Override
    public String toString() {
        return "" + nazwa;
    }
}