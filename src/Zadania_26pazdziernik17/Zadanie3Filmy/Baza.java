package Zadania_26pazdziernik17.Zadanie3Filmy;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Baza {
    private Set<Miasto> miasta = new HashSet<>();
    private Set<Film> filmy = new HashSet<>();
    private Map<Miasto, Map<Film, Double>> bazaMiastoFilmZysk = new HashMap<>();


    public double zyskWMiescie(Miasto miasto, Film film){
        double widzowie = 0;
        if(film.getNazwa().replaceAll(" ", "").length() > 12){
            widzowie += miasto.getPopulacja() * 5 / 100;
        }
        else{
            widzowie += miasto.getPopulacja() * 8 / 100;
        }
        if(film.getNazwa().charAt(0) == 'B'){
            widzowie += miasto.getPopulacja() * 3 / 100;
        }
        if(film.getNazwa().contains("ki")){
            widzowie += miasto.getPopulacja() * 6 / 100;
        }
        if(film.getCena() > 40){
            widzowie = miasto.getPopulacja() / 100;
        }
        return widzowie * (double) film.getCena();
    }

    public void fillData(){
        for (Miasto miasto : miasta) {
            for (Film film : filmy) {
                if(!bazaMiastoFilmZysk.containsKey(miasto)) {
                    bazaMiastoFilmZysk.put(miasto, new HashMap<Film, Double>());
                    bazaMiastoFilmZysk.get(miasto).put(film, zyskWMiescie(miasto, film));
                } else bazaMiastoFilmZysk.get(miasto).put(film, zyskWMiescie(miasto, film));
            }
        }
    }

    public void wczytajPliki(){
        File films = new File("/Users/marekdybusc/IdeaProjects/Programowanie1Divo/Programowanie1/src/Zadania_26pazdziernik17/Zadanie3Filmy/Filmy");
        File miasts = new File("/Users/marekdybusc/IdeaProjects/Programowanie1Divo/Programowanie1/src/Zadania_26pazdziernik17/Zadanie3Filmy/Miasta");

        try (Scanner filmskan = new Scanner(films); Scanner miastoSkan = new Scanner(miasts)) {
            while (filmskan.hasNextLine()){
                filmy.add(new Film(filmskan.nextLine()));
            }while (miastoSkan.hasNextLine()){
                String[] inputArgs = miastoSkan.nextLine().split(" ");
                miasta.add(new Miasto(inputArgs[0], Integer.parseInt(inputArgs[1])));
            }
        }catch (FileNotFoundException fnfe){
            System.out.println("NoFile.");
        }
    }

    public Set<Miasto> getMiasta() {
        return miasta;
    }

    public void setMiasta(Set<Miasto> miasta) {
        this.miasta = miasta;
    }

    public Set<Film> getFilmy() {
        return filmy;
    }

    public void setFilmy(Set<Film> filmy) {
        this.filmy = filmy;
    }

    public Map<Miasto, Map<Film, Double>> getBazaMiastoFilmZysk() {
        return bazaMiastoFilmZysk;
    }

    public void setBazaMiastoFilmZysk(Map<Miasto, Map<Film, Double>> bazaMiastoFilmZysk) {
        this.bazaMiastoFilmZysk = bazaMiastoFilmZysk;
    }
}
