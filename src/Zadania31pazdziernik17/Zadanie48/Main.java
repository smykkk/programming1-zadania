package Zadania31pazdziernik17.Zadanie48;

import java.io.File;

public class Main {
    public static void main(String[] args) {

        File file = new File("/Users/marekdybusc/IdeaProjects/Programowanie1Divo/Programowanie1/src");
        System.out.println(file.getName());
        tree("\t", file);

    }

    private static void tree(String whitespace, File file){

        try {
            for (File podplik : file.listFiles()) {
                if(podplik.isDirectory()) {
                    System.out.println(whitespace + podplik.getName());
                    tree((whitespace + "\t"), podplik);
                }
                else System.out.println(whitespace + podplik.getName());
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
