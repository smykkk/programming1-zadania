package Zadania31pazdziernik17.Zadanie49;

import java.io.*;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner skan = new Scanner(System.in);
        System.out.println("Enter source file path");
        File input = new File(skan.nextLine());
        System.out.println("Enter target file path");
        File output = new File(skan.nextLine());
        Scanner read = new Scanner(input.getAbsoluteFile());
        PrintWriter writer = new PrintWriter(new FileWriter(output.getAbsoluteFile()));
        while (read.hasNextLine()){
            writer.write(read.nextLine());
        }
        writer.flush();
    }
}
