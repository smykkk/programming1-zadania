package Zadania_4listopad17.NaWeekendAmen;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Magazyn m = new Magazyn();
        File file = new File("/Users/marekdybusc/IdeaProjects/Programowanie1Divo/Programowanie1/src/Zadania_4listopad17/NaWeekendAmen/Instrukcje");


        String inputLine = "";

        try(Scanner skan = new Scanner(file)) {
//            while (!inputLine.equals("quit")){
           while (skan.hasNextLine()){
//                System.out.println("Podaj komende: dodaj + nazwa + cena + ilosc + data/ usun + nazwa / sprawdz");
                inputLine = skan.nextLine();
                String[] inputArgs = inputLine.split(" ");
                if(inputArgs[0].equals("dodaj")){
                    try {
                        String nazwa = inputArgs[1];
                        Double cena = Double.parseDouble(inputArgs[2]);
                        int ilosc = Integer.parseInt(inputArgs[3]);
                        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd-HH:mm");
                        LocalDateTime dataWaznosci = LocalDateTime.parse(inputArgs[4], dtf);
                        m.dodaj(nazwa, cena, ilosc, dataWaznosci);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
                else if (inputArgs[0].equals("usun")){
                    try {
                        m.usun(inputArgs[1]);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
                else if(inputArgs[0].equals("sprawdz")){
                    m.sprawdz();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
