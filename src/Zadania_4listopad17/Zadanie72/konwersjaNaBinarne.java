package Zadania_4listopad17.Zadanie72;

import java.util.Scanner;
import java.util.Stack;

public class konwersjaNaBinarne {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        System.out.println("Podaj liczbę do przekonwertowania: ");
        int decimal = skan.nextInt();
        Stack<Integer> binary = new Stack<>();

        binary = binaryBackStack(decimal);

        System.out.println("Stack: " + binary);
        System.out.println("BinaryNumber: " + reverseStackAsNumber(binary));

    }

    private static Stack<Integer> binaryBackStack(int decimal) {
        Stack<Integer> tempStack = new Stack<>();
        int temp = decimal;
        while (temp != 0) {
            int reszta = temp % 2;
            tempStack.push(reszta);
            temp /= 2;
        }
        return tempStack;
    }

    private static int reverseStackAsNumber(Stack<Integer> binary) {
        StringBuilder s = new StringBuilder();
        while (!binary.empty()){
            s.append(binary.pop().toString());
        }
        return Integer.parseInt(s.toString());
    }
}
