package Zadania2listopada2017;

public class Main {
    public static void main(String[] args) {
        QueueElement<Integer> queueElement1 = new QueueElement<>(2, 2);
        QueueElement<Integer> queueElement2 = new QueueElement<>(1, 1);
        QueueElement<Integer> queueElement4 = new QueueElement<>(3, 3);
        QueueElement<Integer> queueElement3 = new QueueElement<>(4, 4);

        MyPriorityQueue mpq = new MyPriorityQueue(true);
        MyPriorityQueue mpq2 = new MyPriorityQueue(false);
        mpq.push(queueElement1);
        mpq.print();
        mpq.push(queueElement2);
        mpq.print();
        mpq.push(queueElement3);
        mpq.print();
        mpq.push(queueElement4);
        mpq.print();

        System.out.println();

        mpq2.push(queueElement1);
        mpq2.print();
        mpq2.push(queueElement2);
        mpq2.print();
        mpq2.push(queueElement3);
        mpq2.print();
        mpq2.push(queueElement4);
        mpq2.print();
    }
}
