package Zadania2listopada2017;

import java.util.ArrayList;
import java.util.List;

public class MyPriorityQueue {
    private List<QueueElement> queue = new ArrayList<>();
    private boolean fromHighest;

    public MyPriorityQueue(boolean fromHighest) {
        this.fromHighest = fromHighest;
    }

    public void push(QueueElement element){
        if (!fromHighest) {
            if (!queue.isEmpty()) {
                int iterator = 0;
                while (iterator < queue.size()){
                    if(queue.get(iterator).getPriority() > element.getPriority()){
                        queue.add(iterator, element);
                        break;
                    }
                    if(iterator == queue.size() -1){
                        queue.add(element);
                        break;
                    }
                    iterator++;
                }
            }
            else queue.add(element);
        } else {
            if (!queue.isEmpty()) {
                int iterator = 0;
                while (iterator < queue.size()){
                    if(queue.get(iterator).getPriority() < element.getPriority()){
                        queue.add(iterator, element);
                        break;
                    }
                    if(iterator == queue.size() -1){
                        queue.add(element);
                        break;
                    }
                    iterator++;
                }
            }
            else queue.add(element);
        }
    }

    public QueueElement pop(){
        if (fromHighest){
            QueueElement temp = queue.get(0);
            queue.remove(temp);
            return temp;
        }
        else {
            QueueElement temp = queue.get(queue.size() -1);
            queue.remove(temp);
            return temp;
        }
    }

    public int size(){
        return queue.size();
    }

    public void print(){
        System.out.print("[");
        for (QueueElement element : queue) {
            System.out.print(" " + element.getObject() + " ");
        }
        System.out.print("]\n");
    }

    @Override
    public String toString() {
        return "MyPriorityQueue{" +
                "queue=" + queue +
                '}';
    }
}
