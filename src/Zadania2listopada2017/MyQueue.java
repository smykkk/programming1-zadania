package Zadania2listopada2017;

import java.util.ArrayList;
import java.util.List;

public class MyQueue<T> {
    private List<T> queue = new ArrayList<>();

    public void pushBack(T element){
        queue.add(element);
    }

    public T popFront(){
        if (!queue.isEmpty()) {
            T temp = queue.get(0);
            queue.remove(0);
            return temp;
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public T peek(){
        if (!queue.isEmpty()) {
            return queue.get(0);
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public int size(){
        return queue.size();
    }

    public void pushFront(T element){
        queue.add(0, element);
    }

    public T popBack(){
        if (!queue.isEmpty()) {
            T temp = queue.get(queue.size() -1);
            queue.remove(temp);
            return temp;
        } else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }
}
