package Zadania3listopada17.Zadanie71;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Magazyn {
    private int dlugosc;
    private Produkt[] tablicaProduktow;

    public Magazyn(int dlugosc) {
        this.dlugosc = dlugosc;
        this.tablicaProduktow = new Produkt[dlugosc];
    }

    public void addProdukt(String nazwa, int cena, int ilosc){
        int counter = 0;

        for (int i = 0; i < dlugosc; i++) {
            if (tablicaProduktow[i] != null) counter++;
        }
        if(counter < dlugosc){
            tablicaProduktow[counter] = new Produkt(nazwa, cena, ilosc);
        }
        else System.out.println("Magazyn pełny.");

    }

    public void printTablica(Produkt[] tablica){
        for (int i = 0; i < dlugosc; i++) {
            if (tablica[i] != null) {
                Produkt p = tablica[i];
                System.out.printf("%17s -> | ilosc: %3s | cena: %3s", p.getNazwa(), p.getIlosc(), p.getCena());
                System.out.println();
            }
        }
    }

    public void printLista(List<Produkt> tablica){
        for (int i = 0; i < dlugosc; i++) {
            if (tablica.get(i) != null) {
                Produkt p = tablica.get(i);
                System.out.printf("%17s -> | ilosc: %3s | cena: %3s", p.getNazwa(), p.getIlosc(), p.getCena());
                System.out.println();
            }
        }
    }


    public List<Produkt> sortujCena(){
        List<Produkt> tempLista = Arrays.asList(tablicaProduktow);
        for (int j = tempLista.size() - 1; j > -1; j--) {
            for (int i = 0; i < j; i++) {
                if(tempLista.get(i).getCena() > tempLista.get(i + 1).getCena()){
                    Produkt tmp1 = tempLista.get(i);
                    tempLista.set(i, tempLista.get(i + 1));
                    tempLista.set(i+1, tmp1);
                }
            }
        }
        return tempLista;
        }

        public List<Produkt> sortujIlosc(){
        List<Produkt> tempLista = Arrays.asList(tablicaProduktow);
        for (int j = tempLista.size() - 1; j > -1; j--) {
            for (int i = 0; i < j; i++) {
                if(tempLista.get(i).getIlosc() > tempLista.get(i + 1).getIlosc()){
                    Produkt tmp1 = tempLista.get(i);
                    tempLista.set(i, tempLista.get(i + 1));
                    tempLista.set(i+1, tmp1);
                }
            }
        }
        return tempLista;
        }

        public List<Produkt> sortujNazwa(){
        List<Produkt> tempLista = Arrays.asList(tablicaProduktow);
        for (int j = tempLista.size() - 1; j > -1; j--) {
            for (int i = 0; i < j; i++) {
                if(tempLista.get(i).getNazwa().compareTo(tempLista.get(i + 1).getNazwa()) >= 0){
                    Produkt tmp1 = tempLista.get(i);
                    tempLista.set(i, tempLista.get(i + 1));
                    tempLista.set(i+1, tmp1);
                }
            }
        }
        return tempLista;
        }

    public Produkt[] getTablicaProduktow() {
        return tablicaProduktow;
    }
}
