package Zadania3listopada17;

import java.util.ArrayList;
import java.util.List;

public class MyBubbleSort {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>();

        lista.add(2);
        lista.add(4);
        lista.add(9);
        lista.add(1);
        lista.add(5);
        lista.add(3);
        lista.add(6);

        for (int j = lista.size() - 1; j > -1; j--) {
            for (int i = 0; i < j; i++) {
                if(lista.get(i) > lista.get(i + 1)){
                    Integer tmp1 = lista.get(i);
                    lista.set(i, lista.get(i + 1));
                    lista.set(i+1, tmp1);
                }
            }
        }

        System.out.println(lista);
    }
}
