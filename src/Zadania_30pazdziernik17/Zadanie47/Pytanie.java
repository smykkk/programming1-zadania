package Zadania_30pazdziernik17.Zadanie47;

public class Pytanie {
    private int numerOdpowiedzi;
    private String pytanie;

    public Pytanie(int numerOdpowiedzi, String pytanie) {
        this.numerOdpowiedzi = numerOdpowiedzi;
        this.pytanie = pytanie;
    }

    public int getNumerOdpowiedzi() {
        return numerOdpowiedzi;
    }

    public void setNumerOdpowiedzi(int numerOdpowiedzi) {
        this.numerOdpowiedzi = numerOdpowiedzi;
    }

    public String getPytanie() {
        return pytanie;
    }

    public void setPytanie(String pytanie) {
        this.pytanie = pytanie;
    }
}
