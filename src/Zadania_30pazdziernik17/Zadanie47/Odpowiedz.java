package Zadania_30pazdziernik17.Zadanie47;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Odpowiedz {
    List<String> odpowiedzi = new ArrayList<>();

    public List<String> getOdpowiedzi() {
        return odpowiedzi;
    }

    public void addOdpowiedz(String odpowiedz){
        odpowiedzi.add(odpowiedz);
    }

    public String getOdpowiedz(Integer index){
        return odpowiedzi.get(index);
    }
}
