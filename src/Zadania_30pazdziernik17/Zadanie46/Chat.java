package Zadania_30pazdziernik17.Zadanie46;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Chat {

    //time -nie wyswietlac, zapisywac
    //przejsc do ostatniej linijki
    //wyswietla wtedy kiedy przyjdzie nowa

    private static File file = new File("/Users/marekdybusc/IdeaProjects/Programowanie1Divo/Programowanie1/src/Zadania_30pazdziernik17/Zadanie46/xchange.txt");

    public void sendMessage(String line){
        try(PrintWriter fw = new PrintWriter(new FileWriter(file, true))){
            fw.println(line);

        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public String getMessage(){
        String line = "";
        try (Scanner skan = new Scanner(file)) {
            while (skan.hasNextLine()){
                line = skan.nextLine();
            }
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return line;
    }
}
